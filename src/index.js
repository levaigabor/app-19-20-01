// import './style.scss';
import './assets/scripts/main.js'
import ApexCharts from 'apexcharts';
if (module.hot) {
  module.hot.accept()
}

document.onreadystatechange = function () {
  if (document.readyState === 'interactive') {
    document.getElementById("defaultTab").click();
    const sideNavButton = document.getElementById('sideNavButton');
    const sideNav = document.getElementById('sideNav');
    const content = document.getElementById('content');
    sideNavButton.addEventListener('click', () => {
      sideNav.classList.toggle('isOpen');
      content.classList.toggle('sideNavIsOpen');
      sideNavButton.classList.toggle('sideNavIsOpen');
    })
    sideNav.classList.remove('isOpen');
    content.classList.remove('sideNavIsOpen');
    sideNavButton.classList.remove('sideNavIsOpen');
  }
  else if (document.readyState === 'complete') {
    summaryChart.render();
    btcChart.render();
    ethChart.render();
    xrpChart.render();
  }
}

let summaryChartOptions = {
  chart: {
      height: 350,
      type: 'area',
  },
  dataLabels: {
      enabled: false
  },
  stroke: {
      curve: 'smooth'
  },
  series: [{
      name: 'Bitcoin',
      data: [31, 40, 28, 51, 42, 109, 100]
  }, {
      name: 'Ethereum',
      data: [11, 32, 45, 32, 34, 52, 41]
  },{
      name: 'XRP',
      data: [1, 12, 5, 22, 14, 32, 21]
  }],

  xaxis: {
      type: 'datetime',
      categories: ["2018-09-19T00:00:00", "2018-09-19T01:30:00", "2018-09-19T02:30:00", "2018-09-19T03:30:00", "2018-09-19T04:30:00", "2018-09-19T05:30:00", "2018-09-19T06:30:00"],                
  },
  tooltip: {
      x: {
          format: 'dd/MM/yy HH:mm'
      },
  }
}

let btcChartOptions = {
  chart: {
      height: 160,
      type: 'area',
      sparkline: {
        enabled: true
      },
  },
  fill: {
    opacity: 1,
  },
  dataLabels: {
      enabled: false
  },
  stroke: {
      curve: 'straight'
  },
  series: [{
      name: 'Bitcoin',
      data: [31, 40, 28, 51, 42, 109, 100]
  }],
  title: {
    text: '$1,000',
    offsetX: 30,
    style: {
      fontSize: '24px',
      cssClass: 'apexcharts-yaxis-title'
    }
  },
  subtitle: {
    text: 'Bitcoin',
    offsetX: 30,
    style: {
      fontSize: '14px',
      cssClass: 'apexcharts-yaxis-title'
    }
  },
  xaxis: {
      type: 'datetime',
      categories: ["2018-09-19T00:00:00", "2018-09-19T01:30:00", "2018-09-19T02:30:00", "2018-09-19T03:30:00", "2018-09-19T04:30:00", "2018-09-19T05:30:00", "2018-09-19T06:30:00"],                
  },
  tooltip: {
      x: {
          format: 'dd/MM/yy HH:mm'
      },
  }
}

let ethChartOptions = {
  chart: {
    height: 160,
    type: 'area',
    sparkline: {
      enabled: true
    },
  },
  fill: {
    opacity: 1,
  },
  dataLabels: {
      enabled: false
  },
  stroke: {
      curve: 'straight'
  },
  title: {
    text: '$2,300',
    offsetX: 30,
    style: {
      fontSize: '24px',
      cssClass: 'apexcharts-yaxis-title'
    }
  },
  subtitle: {
    text: 'Ethereum',
    offsetX: 30,
    style: {
      fontSize: '14px',
      cssClass: 'apexcharts-yaxis-title'
    }
  },
  series: [ {
      name: 'Ethereum',
      data: [11, 32, 45, 32, 34, 52, 41]
  }],

  xaxis: {
      type: 'datetime',
      categories: ["2018-09-19T00:00:00", "2018-09-19T01:30:00", "2018-09-19T02:30:00", "2018-09-19T03:30:00", "2018-09-19T04:30:00", "2018-09-19T05:30:00", "2018-09-19T06:30:00"],                
  },
  tooltip: {
      x: {
          format: 'dd/MM/yy HH:mm'
      },
  }
}

let xrpChartOptions = {
  chart: {
    height: 160,
    type: 'area',
    sparkline: {
      enabled: true
    },
  },
  fill: {
    opacity: 1,
  },
  dataLabels: {
      enabled: false
  },
  stroke: {
      curve: 'straight'
  },
  series: [{
      name: 'XRP',
      data: [1, 12, 5, 22, 14, 32, 21]
  }],
  title: {
    text: '$2,700',
    offsetX: 30,
    style: {
      fontSize: '24px',
      cssClass: 'apexcharts-yaxis-title'
    }
  },
  subtitle: {
    text: 'XRP',
    offsetX: 30,
    style: {
      fontSize: '14px',
      cssClass: 'apexcharts-yaxis-title'
    }
  },
  xaxis: {
      type: 'datetime',
      categories: ["2018-09-19T00:00:00", "2018-09-19T01:30:00", "2018-09-19T02:30:00", "2018-09-19T03:30:00", "2018-09-19T04:30:00", "2018-09-19T05:30:00", "2018-09-19T06:30:00"],                
  },
  tooltip: {
      x: {
          format: 'dd/MM/yy HH:mm'
      },
  }
}

let summaryChart = new ApexCharts(document.querySelector("#summaryChart"), summaryChartOptions);
let btcChart = new ApexCharts(document.querySelector("#btcChart"), btcChartOptions);
let ethChart = new ApexCharts(document.querySelector("#ethChart"), ethChartOptions);
let xrpChart = new ApexCharts(document.querySelector("#xrpChart"), xrpChartOptions);
